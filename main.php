<?php


$s1 = new Peca_posicionamento("soldado computador");
$s2 = new Peca_posicionamento("soldado computador");
$s3 = new Peca_posicionamento("soldado computador");
$ba = new Peca_posicionamento("bandeira computador");
$es = new Peca_posicionamento("espiao computador");
$b1 = new Peca_posicionamento("bomba computador");
$b2 = new Peca_posicionamento("bomba computador");
$c1 = new Peca_posicionamento("cabo computador");
$c2 = new Peca_posicionamento("cabo computador");
$ma = new Peca_posicionamento("marechal computador");

$timepc = array($s1,$s2,$s3,$es,$c1,$c2,$ma);

shuffle($timepc);



function MontarTabuleiro($ba,$timepc,$b1,$b2){


if(isset($_GET['montar'])) // se o usuário clicou em um dos botões, esse if será chamado
{
	echo  "<center><h1>1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  5</h1></center>";
	echo "<div id='diva' style='position:absolute;left:350px;top:70px;'><h1>A</h1></div>";
	echo "<div id='divb' style='position:absolute;left:350px;top:150px;'><h1>B</h1></div>";
	echo "<div id='diva' style='position:absolute;left:350px;top:230px;'><h1>C</h1></div>";
	echo "<div id='diva' style='position:absolute;left:350px;top:310px;'><h1>D</h1></div>";
	echo "<div id='diva' style='position:absolute;left:350px;top:390px;'><h1>E</h1></div>";
	$lago = rand(1,3); // gera um número aleatório para o lago (não podendo ser nos cantos pois precisa dividir o mapa em 2 caminhos)
	$bandeira_pc = rand(0,1);	 // gera a coluna em que a bandeira vai ficar (na linha 0)
	if($bandeira_pc == 1)
	{	
		$bandeira_pc = 4;
		$bomba1_pc = 3;
		$bomba2_pc = 4; 
	}
	else
	{
		$bomba1_pc = 1;
		$bomba2_pc = 0;
	}
	
	echo "<div id='tabuleiro' style='margin:0px auto 0px auto;'>";
	if($_GET['montar'] == "manualmente") // se ele escolheu organizar manualmente, esse if será chamado
	{
		$cont = 0;
		for($i=0; $i<5; $i++)
		{
			echo "<div id='linha_".$i."' class='linha'>";
			for ($j=0; $j<5; $j++)
			{
				
				$indice_casa = $j+1; //gerando índice para as casas	

				if($i==0 && $j == $bomba1_pc) // incluindo a bandeira do computador (na 1° linha, obrigatoriamente)
				{				
					echo "<div id='a".$indice_casa."' class='casa ".$b1->classe."'>" ;	
					echo "</div>";			
				}	
				else if($i==0 && $j == $bandeira_pc) // incluindo a bandeira do computador (na 1° linha, obrigatoriamente)
				{				
					echo "<div id='a".$indice_casa."' class='casa ".$ba->classe."'>" ;	
					echo "</div>";			
				}	
				else if($i == 0 && $j != $bandeira_pc) // incluindo peças do computador na 1° linha
				{	
					echo "<div id='a".$indice_casa."' class='casa ".$timepc[$cont]->classe."'>" ;
					$cont++;
					echo "</div>";
				}
				else if($i == 1 && $j == $bomba2_pc) // incluindo peças do computador na 2° linha
				{
					echo "<div id='b".$indice_casa."' class='casa ".$b2->classe."'>" ;
					echo "</div>";
				}
				else if($i == 1) // incluindo peças do computador na 2° linha
				{
					echo "<div id='b".$indice_casa."' class='casa ".$timepc[$cont]->classe."'>" ;
					$cont++;
					echo "</div>";
				}					
				else if($i==2 && $j==$lago) // incluindo o lago
				{
					echo "<div id='c".$indice_casa."' class='casa lago'/>";
					echo "</div>"; 		
				}
				else if($i==2) // incluindo o lago
				{
					echo "<div id='c".$indice_casa."' class='casa'/>";
					echo "</div>"; 		
				}
				else if($i==3)
				{
					echo "<div id='d".$indice_casa."' class='casa'/>";
					echo "</div>"; 

				}
				else // incluindo casas vazias
				{			
					echo "<div id='e".$indice_casa."' class='casa'/>";
					echo "</div>"; 
				}						
			}	
			echo "</div>";	
		}
		echo "<div id='linha_6' class='linha_inv'>";
		echo "<center>";
		echo "<button id='jogar' class='botaoentrar'> Jogar </button>";
		echo "<button id='debug' class='botaoentrar'> DEBUG </button>"; 
		echo "</center>";
		echo "</div>";
		echo "<div id='linha_5' class ='linha_inv'>";
		echo "<div id='jogador_soldado1' class='soldado jogador' ></div>";
		echo "<div id='jogador_soldado2' class='soldado jogador' ></div>";
		echo "<div id='jogador_soldado3' class='soldado jogador' ></div>";
		echo "<div id='jogador_bandeira' class='bandeira jogador' ></div>";
		echo "<div id='jogador_espiao' class='espiao jogador' ></div>";		
		echo "<div id='jogador_bomba1' class='bomba jogador' ></div>";
		echo "<div id='jogador_bomba2' class='bomba jogador' ></div>";
		echo "<div id='jogador_cabo1' class='cabo jogador' ></div>";
		echo "<div id='jogador_cabo2' class='cabo jogador' ></div>";
		echo "<div id='jogador_marechal' class='marechal jogador'> </div>";
		echo "</div>";		
		echo "</div> </center>";
	}
	else if($_GET['montar'] == "automaticamente") // se o jogador escolher organizar automaticamente, esse if será chamado
	{
		echo "<div id='diva' style='position:absolute;left:350px;top:70px;'><h1>A</h1></div>";
		echo "<div id='divb' style='position:absolute;left:350px;top:150px;'><h1>B</h1></div>";
		echo "<div id='diva' style='position:absolute;left:350px;top:230px;'><h1>C</h1></div>";
		echo "<div id='diva' style='position:absolute;left:350px;top:310px;'><h1>D</h1></div>";
		echo "<div id='diva' style='position:absolute;left:350px;top:390px;'><h1>E</h1></div>";
		
		$s1j = new Peca_posicionamento("soldado jogador");
		$s2j = new Peca_posicionamento("soldado jogador");
		$s3j = new Peca_posicionamento("soldado jogador");
		$baj = new Peca_posicionamento("bandeira jogador");
		$esj = new Peca_posicionamento("espiao jogador");
		$b1j = new Peca_posicionamento("bomba jogador");
		$b2j = new Peca_posicionamento("bomba jogador");
		$c1j = new Peca_posicionamento("cabo jogador");
		$c2j = new Peca_posicionamento("cabo jogador");
		$maj = new Peca_posicionamento("marechal jogador");

		$timejogador = array($s1j,$s2j,$s3j,$esj,$c1j,$c2j,$maj); // gera um array com as peças do jogador
		shuffle($timejogador); // reorganiza o array do jogador aleatoriamente

		$bandeira_jogador = rand(0,1); // gera um índice para a bandeira do jogador
		if($bandeira_jogador == 1)
		{
			$bandeira_jogador = 4;
			$bomba1 = 4;
			$bomba2 = 3;
		} 
		else
		{
			$bomba1 = 0;
			$bomba2 = 1;
		}
		$cont = 0;
		$contj=0;

		for($i=0; $i<5; $i++)
		{		
			echo "<div id='linha_".$i."' class='linha'>";
			for ($j=0; $j<5; $j++)
			{
				$indice_casa = $j+1; //gerando índice para as casas

				if($i==0 && $j == $bomba1_pc) // incluindo a bandeira do computador (na 1° linha, obrigatoriamente)
				{				
					echo "<div id='a".$indice_casa."' class='casa ".$b1->classe."'>" ;	
					echo "</div>";			
				}	
				else if($i==0 && $j == $bandeira_pc) // incluindo a bandeira do computador (na 1° linha, obrigatoriamente)
				{				
					echo "<div id='a".$indice_casa."' class='casa ".$ba->classe."'>" ;	
					echo "</div>";			
				}	
				else if($i == 0 && $j != $bandeira_pc) // incluindo peças do computador na 1° linha
				{	
					echo "<div id='a".$indice_casa."' class='casa ".$timepc[$cont]->classe."'>" ;
					$cont++;
					echo "</div>";
				}
				else if($i == 1 && $j == $bomba2_pc) // incluindo peças do computador na 2° linha
				{
					echo "<div id='b".$indice_casa."' class='casa ".$b2->classe."'>" ;
					echo "</div>";
				}
				else if($i == 1) // incluindo peças do computador na 2° linha
				{
					echo "<div id='b".$indice_casa."' class='casa ".$timepc[$cont]->classe."'>" ;
					$cont++;
					echo "</div>";
				}			
				else if($i==2 && $j==$lago)  // incluindo o lago
				{
					echo "<div id='c".$indice_casa."' class='casa lago'/>"; 
					echo "</div>"; 		
				}
				else if($i == 3 && $j == $bomba1)	// incluindo bandeira do jogador				
				{
					echo "<div id='d".$indice_casa."' class='casa ".$b1j->classe."'>" ;	
					echo "</div>";	
				}
				else if($i==3) // incluindo peças do jogador na 2° linha
				{			
					echo "<div id='d".$indice_casa."' class='casa ".$timejogador[$contj]->classe."'/>";
					$contj++;
					echo "</div>"; 
				}
				else if($i == 4 && $j == $bandeira_jogador)	// incluindo bandeira do jogador				
				{
					echo "<div id='e".$indice_casa."' class='casa ".$baj->classe."'>" ;	
					echo "</div>";	
				}
				else if($i == 4 && $j == $bomba2)	// incluindo bandeira do jogador				
				{
					echo "<div id='e".$indice_casa."' class='casa ".$b2j->classe."'>" ;	
					echo "</div>";	
				}
				else if($i == 4) // incluindo peças do jogador na 1° linha
				{
					echo "<div id='e".$indice_casa."' class='casa ".$timejogador[$contj]->classe."'/>";
					$contj++;
					echo "</div>"; 
				}
				else // incluindo casas vazias
				{
					echo "<div id='c".$indice_casa."' class='casa'/>";
					echo "</div>";
				}
			}

			echo "</div>";
		}

		echo "<div id='linha_6' class='linha_inv'>";
		echo "<center>";
		echo "<button id='jogar' class='botaoentrar'> Jogar </button>"; 
		echo "<button id='debug' class='botaoentrar'> DEBUG </button>";
		echo "</center>"; 
		echo "</div>";
		echo "</div>";	
}

}
	else // criação dos botões caso o jogador não tenha selecionado como montar o tabuleiro (1° página)
	{		
		echo "
	    <div id='opcao'>
		<p><h1><center> Bem vindo ao Combate!<br> Como deseja organizar suas peças?</center></h1></p>
		<center><button id='manualmente' class='botaoentrar'> Manualmente </button>
		<button id='automaticamente' class='botaoentrar'> Automaticamente </button></center>
		<br><br><br>
		</div>";
	}

}



MontarTabuleiro($ba,$timepc,$b1,$b2);

?>