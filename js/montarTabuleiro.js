$(function(){

var peca_selecionada = null;
var classe_selecionada = null;
var array_pecas = new Array;
var contador = 0;
var bandeira_posicionada = false;

$("#manualmente").click(function() //redireciona a página para organizar aleatoriamente as peças
{
    location.href="combate.php?montar=manualmente";
});


$("#automaticamente").click(function() // redireciona a página para organizar automaticamente as peças
{
    location.href="combate.php?montar=automaticamente";
});

$("#debug").click(function() // redireciona a página para organizar automaticamente as peças
{
    var peca;

    $(".casa").each( function(index, value)
    {
        var classecasa = $(this).attr('class');
        $(this).removeClass('computador');
        $(this).removeClass('jogador');
        if(classecasa == "casa")
        {
            peca = 'nenhuma';
            $(this).append("<div class='debug'> <center> Casa:<br>"+$(this).attr('id')+" <br> Peça:<br>"+peca+" </center> </div>");
            $(this).removeClass('casa');
            $(this).addClass('debug');
        }
        else
        {
            $(this).removeClass('casa');
            peca = $(this).attr('class');
            $(this).removeClass(peca);
            $(this).addClass('debug');
            $(this).append("<center> Casa:<br>"+$(this).attr('id')+" <br> Peça:<br>"+peca+" </center>");
        }
    });
   $("#debug").remove();
   $("#linha_6").append("<center> <button onClick='window.location.reload();' class='botaoentrar'>Posicionar</button> </center>");
   $('#linha_5').remove();
   $('#jogar').remove();
});

$("#posicionar").click(function()
{
    location.reload();
});

$("#linha_5 div").click(function() // função chamada ao clicar em uma peça para posicioná-la 
{
    peca_selecionada = $(this).attr('id'); // pega o id da peça selecionada para posicionamento
    classe_selecionada = $(this).attr('class'); // pega a classe da peça selecionada para posicionamento

});

$(".casa").click(function() // função chamada ao clicar em uma casa
    {
        casa_selecionada = $(this).attr('class');
        
        if(peca_selecionada != null) //se uma peça for selecionada antes de clicar na casa, ele move a peça para lá
        {
            if(casa_selecionada == "casa" || casa_selecionada == "casa selecionada")
            {             
                if(classe_selecionada == "bandeira jogador") // condição para posicionamento da bandeira
                {
                    if($(this).parent().attr('id') == "linha_4")
                    {
                        $('#'+peca_selecionada).remove();
                        $(this).addClass(classe_selecionada);
                        bandeira_posicionada = true;

                    }
                    else
                    {
                        alert("Você só pode posicionar a bandeira na linha E");
                    }
                }  
                else            
                {
                    if($(this).parent().attr('id') == "linha_4" || $(this).parent().attr('id') == "linha_3") // condição para posicionamento das peças em geral
                    { 
                        if($(this).parent().attr('id') == "linha_4" && contador == 4 && bandeira_posicionada == false) 
                        {
                            alert('Sò há um espaço e a bandeira não foi posicionada. Posicione-a');
                        }
                        else if($(this).parent().attr('id') == "linha_3")
                        {                
                            $('#'+peca_selecionada).remove();
                            $(this).addClass(classe_selecionada);
                        }
                        else if($(this).parent().attr('id') == "linha_4")
                        {
                            contador++;
                            $('#'+peca_selecionada).remove();
                            $(this).addClass(classe_selecionada);
                        }
                    }
                    else
                    {
                        alert("Você só pode posicionar peças nas linhas D e E");
                    }
                }
                peca_selecionada = null; // define como null para dizer que nenhuma peça está selecionada depois do movimento
            } 
            else
            {
                alert('Essa casa já possui peças');
            } 
        }
        else
        {           
            $(".selecionada").removeClass("selecionada");
            $(this).addClass("selecionada"); 
        }
  
    });

$("#jogar").click(function() //redireciona a página para organizar aleatoriamente as peças
{
    var vazio = "não";
    var cont = 0;
    var cont1 = 0;
    var cont2 = 0;
    var cont3 = 0;
    var cont4 = 0;

    $("#linha_3 > div").each( function(index, value)
    {
        if($(this).attr('class') == "casa")
        {
            vazio = "sim";
        }     
    });
    $("#linha_4 > div").each( function(index, value)
    {
        if($(this).attr('class') == "casa")
        {
            vazio = "sim";
        }          
    });

    if(vazio == "não")
    {

    $(".selecionada").removeClass('selecionada');
    $(".jogador").removeClass('casa');
    $(".computador").removeClass('casa');
    $(".jogador").removeClass('jogador');
    $(".computador").removeClass('computador');

    $("#linha_6").append('<form name="envio" method="POST" id="formulario" action="jogo.php">');

    $("#linha_0 > div").each( function(index, value)
    {
        $("#formulario").append('<input type="hidden" name="'+$(this).attr('id')+'" value="'+$(this).attr('class')+'" >');
        cont++;     
    });
    $("#linha_1 > div").each( function(index, value)
    {
        $("#formulario").append('<input type="hidden" name="'+$(this).attr('id')+'" value="'+$(this).attr('class')+'" >');
        cont1++;     
    });
    $("#linha_2 > div").each( function(index, value)
    {
        $("#formulario").append('<input type="hidden" name="'+$(this).attr('id')+'" value="'+$(this).attr('class')+'" >');
        cont2++;     
    });
    $("#linha_3 > div").each( function(index, value)
    {
        $("#formulario").append('<input type="hidden" name="'+$(this).attr('id')+'" value="'+$(this).attr('class')+'" >');
        cont3++;
        if($(this).attr('class') == "casa")
        {
            vazio = "sim";
        }     
    });
    $("#linha_4 > div").each( function(index, value)
    {
        $("#formulario").append('<input type="hidden" name="'+$(this).attr('id')+'" value="'+$(this).attr('class')+'" >');
        if($(this).attr('class') == "casa")
        {
            vazio = "sim";
        }          
    });

    $("#linha_6").append('</form>');
    document.envio.submit();

}
    else
    {
        alert('Posicione todas as peças');
    }

    
});



});
