<?php

Class Tabuleiro{
	
	public $tabela;
	private $dicas;
	private $on;
	private $tabelainicial;

	public function __construct($tabela)
	{
		$this->tabela = $tabela;
		$this->tabelainicial = $tabela;
		$this->dicas = 2;
		$this->on = true;

		foreach($tabela as $key => $value)
		{
			if($value->classe == 'bomba' && $value->lado == 'computador')
			{
				$this->bombas[$key] = 'sim';
			}
			else
			{
				$this->bombas[$key] = 'não';
			}
		}
	}

	public function redefinir()
	{	
		$this->tabela = $this->tabelainicial;
		$this->dicas = 2;
		$this->on = true;
	}

	public function contarClasses($lado)
	{
		$contsoldado = 0;
		$contmarechal = 0;
		$contbandeira = 0;
		$contbomba = 0;
		$contcabo = 0;
		$contespiao = 0;

		foreach($this->tabela as $key => $value)
		{
			if(is_a($value, 'Peca') || is_a($value, 'Peca_imovel'))
			{
				if($value->lado == $lado)
				{
					if($value->classe == 'soldado')
					{
						$contsoldado++;
					}
					if($value->classe == 'marechal')
					{
						$contmarechal++;
					}
					if($value->classe == 'bandeira')
					{
						$contbandeira++;
					}
					if($value->classe == 'bomba')
					{
						$contbomba++;
					}
					if($value->classe == 'cabo')
					{
						$contcabo++;
					}
					if($value->classe == 'espiao')
					{
						$contespiao++;
					}

				}
			}
		}

		if($contsoldado != 0)
		{
			$arrayclasses['soldado'] = $contsoldado;
		}
		if($contmarechal != 0)
		{
			$arrayclasses['marechal'] = $contmarechal;
		}
		if($contbandeira != 0)
		{
			$arrayclasses['bandeira'] = $contbandeira;
		}
		if($contbomba != 0)
		{
			$arrayclasses['bomba'] = $contbomba;
		}
		if($contcabo != 0)
		{
			$arrayclasses['cabo'] = $contcabo;
		}
		if($contespiao != 0)
		{
			$arrayclasses['espiao'] = $contespiao;
		}

		return $arrayclasses;

	}

	public function Dica($casaselect)
	{
		$casaseparada = explode(" ",$casaselect);

		if($casaseparada[0] == 'a')
		{
			$n = 0;
		}
		else if($casaseparada[0] == 'b')
		{
			$n = 5;
		}
		else if($casaseparada[0] == 'c')
		{
			$n = 10;
		}
		else if($casaseparada[0] == 'd')
		{
			$n = 15;
		}
		else if($casaseparada[0] == 'e')
		{
			$n = 20;
		}

		$casa = $n+$casaseparada[1];

		if($this->bombas[$casa] == 'sim')
		{
			echo "<script> alert('Você achou uma bomba') </script>";
		}
		else
		{
			echo "<script> alert('Não há bombas aqui') </script>";
		}
		$this->dicas--;
	}

	public function contarPecas($lado)
	{
		$contpecas = 0;
		foreach($this->tabela as $key => $value)
		{
			if(is_a($value, 'Peca'))	
			{
				if($value->lado == $lado)
				{
					$contpecas++;
				}
			}
		}
			return $contpecas;
	}

	public function exibirTabuleiro()
	{
	echo "<center><h1>1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  3&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  4&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  5</h1></center>";
	echo "<div id='diva' style='position:absolute;left:380px;top:90px;'><h1>A</h1></div>";
	echo "<div id='divb' style='position:absolute;left:380px;top:170px;'><h1>B</h1></div>";
	echo "<div id='diva' style='position:absolute;left:380px;top:250px;'><h1>C</h1></div>";
	echo "<div id='diva' style='position:absolute;left:380px;top:330px;'><h1>D</h1></div>";
	echo "<div id='diva' style='position:absolute;left:380px;top:410px;'><h1>E</h1></div>";
	echo "<div id='tabuleiro'>";
	$cont = 1;
	for($i=0; $i< 5; $i++)
		{
			
			echo "<div id='linha_".$i."' class='linha'>";
			for ($j=0; $j< 5; $j++)
			{
				
				$indice_casa = $cont;
				if(is_a($this->tabela[$cont], "Peca") || is_a($this->tabela[$cont], "Peca_imovel"))
				{
					if($this->tabela[$cont]->lado == "jogador")
					{
						echo "<div id='".$indice_casa."' class='jogador ".$this->tabela[$cont]->classe."'>";
						echo "</div>";
					}
					else if($this->tabela[$cont]->lado == "computador")
					{
						echo "<div id='".$indice_casa."' class='".$this->tabela[$cont]->classe." computador'>";
						echo "</div>";
					}
				}
				else
				{
					echo "<div id='".$indice_casa."' class='".$this->tabela[$cont]->classe."'>";
					echo "</div>";
				}
				$cont++;
			}
			echo "</div>";		
		}
		echo "<form name='movimentacao' action='jogo.php' method='POST' id='movimento'> </form>";
		
		if($this->dicas > 0)
		{
			echo "<div id='linha_botoes' class='linha_inv'><center>";
			echo "<form name='dica' action='jogo.php' method='POST'> <br> Dica: <input type='text' name='dica'> <button id='dica' > Verificar </button> 
			 </form> </center> </div>"; 
		}
		echo "<div id='linha_sair' class='linha_inv'><center>";
		echo "<button id='sair' class='botaoentrar'> Sair </button>";
		echo "</center> </div>";
		echo "</div>";
		$arraypecasjogador = $this->contarClasses('jogador');
		$arraypecaspc = $this->contarClasses('computador');
		echo "<br> <br> <br> <br> <br> <br> <br> <br> <br> <br> <div id='linha_jogador class='linha_pecas'> <center>";
		foreach($arraypecasjogador as $key => $value)
		{
			echo "<div id='cont ".$key."' class='".$key."_".$value."'> </div>";
		}
		echo "</div>";
		echo "<br> <br> <br> <br> <br> <br> <div id='linha_jogador class='linha_jogador'> <center>";
		foreach($arraypecaspc as $key => $value)
		{
			echo "<div id='cont ".$key."' class='".$key."_".$value."_pc'> </div>";
		}
		echo "</div>";

	}

	public function Movimentar($casaataque,$casaatacada)
	{

	$resultado = 3;
	if($this->tabela[$casaataque]->classe == 'bomba' || $this->tabela[$casaataque]->classe == 'bandeira')
	{
		echo '<script> alert("Você não pode mover essa peça") </script> ';
	}
	else if($this->tabela[$casaataque]->classe == 'soldado')
	{
		$validar = true;
		if(definirLinha($casaataque) == definirLinha($casaatacada))
		{	
			if($casaataque > $casaatacada)
			{
				for($x=$casaatacada+1;$x<$casaataque;$x++)
				{			
					if($this->tabela[$x]->classe != 'casa')
					{
						$validar = false;
					}	
				}
			}
			else
			{
				for($x=$casaataque+1;$x<$casaatacada;$x++)
				{				
					if($this->tabela[$x]->classe != 'casa')
					{
						$validar = false;
					}			
				}
			}				
		}
		else if(($casaataque > $casaatacada))
		{
			if(($casaataque-$casaatacada)%5==0)
			{
				for($x=$casaatacada+1;$x<$casaataque;$x++)
				{
					if(($casaataque-$x)%5==0)
					{
						if($this->tabela[$x]->classe != 'casa')
						{
							$validar = false;
						}
					}
				}
			}
			else
			{
				$validar = false;
			}
		}
		else
		{
			if(($casaatacada-$casaataque)%5==0)
			{
				for($x=$casaataque+1;$x<$casaatacada;$x++)
				{
					if(($casaatacada-$x)%5==0)
					{
						if($this->tabela[$x]->classe != 'casa')
						{
							$validar = false;
						}
					}
				}
			}
			else
			{
				$validar = false;
			}
		}
			
		if($validar == true)
		{
			$resultado = $this->tabela[$casaataque]->Lutar($this->tabela[$casaatacada]);
		}
		else
		{
			$resultado = 3;
		}

	}
	else
	{
		if(definirLinha($casaataque) == definirLinha($casaatacada))
		{
			if($casaatacada == $casaataque+1 || $casaatacada == $casaataque-1)
			{
				$resultado = $this->tabela[$casaataque]->Lutar($this->tabela[$casaatacada]);
			}
			else
			{
				$resultado = 3;
			}
		}
		else
		{
			if($casaatacada == $casaataque+5 || $casaatacada == $casaataque-5)
			{
				$resultado = $this->tabela[$casaataque]->Lutar($this->tabela[$casaatacada]);
			}
			else
			{
				$resultado = 3;
			}
		}
	}

		if($resultado == 0)
		{
			$c = new Objeto('casa');
			$this->tabela[$casaataque] = $c;
			$m = $this->contarPecas('jogador');
			$n = $this->contarPecas('computador');
			if($m > 0)
			{
				if($n > 0)
				{
					$this->MovimentarPC();
				}
				else
				{
					echo "<script> alert('As peças móveis do adversário acabaram, você venceu!') </script>";
					$this->on = false;
					if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
				}	
			}
			else
			{
				echo "<script> alert('Suas peças móveis acabaram, você perdeu..') </script>";
				$this->on = false;
				if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
			}
		}
		else if($resultado == 1)
		{
			$c = new Objeto('casa');
			$this->tabela[$casaatacada] = $this->tabela[$casaataque];
			$this->tabela[$casaataque] = $c;
			$m = $this->contarPecas('jogador');
			$n = $this->contarPecas('computador');
			if($m > 0)
			{
				if($n > 0)
				{
					$this->MovimentarPC();
				}
				else
				{
					echo "<script> alert('As peças móveis do adversário acabaram, você venceu!') </script>";
					$this->on = false;
					if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
				}	
			}
			else
			{
				echo "<script> alert('Suas peças móveis acabaram, você perdeu..') </script>";
				$this->on = false;
				if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
			}
		}
		else if($resultado == 2)
		{
			$c = new Objeto('casa');
			$this->tabela[$casaataque] = $c;
			$this->tabela[$casaatacada] = $c;
			$m = $this->contarPecas('jogador');
			$n = $this->contarPecas('computador');
			if($m > 0)
			{
				if($n > 0)
				{
					$this->MovimentarPC();
				}
				else
				{
					echo "<script> alert('As peças móveis do adversário acabaram, você venceu!') </script>";
					$this->on = false;
					if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
				}	
			}
			else
			{
				echo "<script> alert('Suas peças móveis acabaram, você perdeu..') </script>";
				$this->on = false;
				if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
			}
		}
		else if($resultado == 3)
		{
			echo "<script> alert('Movimento inválido') </script>";
			$this->exibirTabuleiro();
		}
		else if($resultado == 4)
		{		
			echo '<script>alert("Você não pode se mover para o lago");</script>';
			$this->exibirTabuleiro();
		}	
		else if($resultado == 10)
		{
			$this->on = false;
			if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}
		}



}

public function MovimentarPC()
{
	$cont = 0;
	foreach($this->tabela as $indice => $objeto)
	{
		if(is_a($objeto, "Peca"))
		{
			if($objeto->lado == 'computador')
			{					
					$contador = 0;
					for($y=1;$y<=25;$y++)
					{
						if(definirLinha($indice) == definirLinha($y))
						{
							if($y == $indice+1 || $y == $indice-1)
							{
								if(is_a($this->tabela[$y], 'Peca'))
								{
									if($this->tabela[$y]->lado != 'computador')
									{
										$contador++;
									}
								}
								else if(is_a($this->tabela[$y], 'Objeto'))
								{
									if($this->tabela[$y]->classe == 'casa')
									{
										$contador++;
									}
								}
							}		
						}
						else if($y == ($indice+5) || $y == ($indice-5))
						{
								if(is_a($this->tabela[$y], 'Peca'))
								{
									if($this->tabela[$y]->lado != 'computador')
									{
										$contador++;
									}
								}
								else if(is_a($this->tabela[$y], 'Objeto'))
								{
									if($this->tabela[$y]->classe == 'casa')
									{
										$contador++;
									}
								}
								
						}
					}
						if($contador > 0)
						{
							$pecaspc[$cont] = $indice;
							$cont++;
						}
				}
			}
		}

	shuffle($pecaspc);
	$indice = $pecaspc[0];

				if($this->tabela[$indice]->classe == 'soldado')
				{
					$contador = 0;
					for($y=1;$y<=25;$y++)
					{
						$validar = true;
						if(definirLinha($indice) == definirLinha($y))
						{	
							if($indice > $y)
							{
								if($this->tabela[$y]->classe == 'casa lago')
								{
									$validar=false;
								}
								else if(is_a($this->tabela[$y], 'Peca') && $this->tabela[$y]->lado == 'computador')
								{
									$validar = false;
								}
								else if(is_a($this->tabela[$y], 'Peca_imovel') && $this->tabela[$y]->lado == 'computador')
								{
									$validar = false;
								}
								else
								{
								for($x=$y+1;$x<$indice;$x++)
									{
										if(is_a($this->tabela[$x], 'Peca'))
										{							
												$validar = false;
											
										}
										else if(is_a($this->tabela[$x], 'Objeto'))
										{
											if($this->tabela[$x]->classe != 'casa')
											{
												$validar = false;
											}
										}
									}
								}
							}
							else if($indice < $y)
							{
								if($this->tabela[$y]->classe == 'casa lago')
								{
										$validar=false;
								}
								else if(is_a($this->tabela[$y], 'Peca') && $this->tabela[$y]->lado == 'computador')
								{
									$validar = false;
								}
								else if(is_a($this->tabela[$y], 'Peca_imovel') && $this->tabela[$y]->lado == 'computador')
								{
									$validar = false;
								}
								else
								{
									for($x=$indice+1;$x<$y;$x++)
									{

											if(is_a($this->tabela[$x], 'Peca'))
											{
													$validar = false;
											}
											else if(is_a($this->tabela[$x], 'Peca_imovel'))
											{
												$validar = false;
											}
											else if(is_a($this->tabela[$x], 'Objeto'))
											{
												if($this->tabela[$x]->classe != 'casa')
												{
													$validar = false;
												}
											}									
										
									}
								}
							}
							else
							{
								$validar = false;
							}				
						}
						else
						{	
							if(($indice > $y))
							{
								if(($indice-$y)%5==0)
								{
									if($this->tabela[$y]->classe == 'casa lago')
									{
										$validar=false;
									}
									else if(is_a($this->tabela[$y], 'Peca') && $this->tabela[$y]->lado == 'computador')
									{
										$validar = false;
									}
									else if(is_a($this->tabela[$y], 'Peca_imovel') && $this->tabela[$y]->lado == 'computador')
									{
										$validar = false;
									}
									else
									{
									for($x=$y+1;$x<$indice;$x++)
									{
										if(is_a($this->tabela[$x], 'Peca'))
										{
											if($this->tabela[$x]->lado == 'computador')
											{
												$validar = false;
											}
										}
										else if(is_a($this->tabela[$x], 'Objeto'))
										{
											if($this->tabela[$x]->classe != 'casa')
											{
												$validar = false;
											}
										}
									}
								}
								}
								else
								{
									$validar = false;
								}
							}
							else if(($indice < $y))
							{
								if(($y-$indice)%5==0)
								{
									if($this->tabela[$y]->classe == 'casa lago')
									{
										$validar=false;
									}
									else if(is_a($this->tabela[$y], 'Peca') && $this->tabela[$y]->lado == 'computador')
									{
										$validar = false;
									}
									else if(is_a($this->tabela[$y], 'Peca_imovel') && $this->tabela[$y]->lado == 'computador')
									{
										$validar = false;
									}
									else
									{
									for($x=$indice+1;$x<$y;$x++)
									{
										if(($y-$x)%5==0 || $y-$x == 0)
										{
											if(is_a($this->tabela[$x], 'Peca') || is_a($this->tabela[$x], 'Peca_imovel'))
											{
													$validar = false;
											}
											else if(is_a($this->tabela[$x], 'Objeto'))
											{
												if($this->tabela[$x]->classe != 'casa')
												{
													$validar = false;
												}
											}									
										}
									}
								}
								}
								else
								{
									$validar = false;
								}
							}
						}

						if($validar == true)
						{
								$movimentos[$contador] = $y;
								$contador++;
						}
						
					}
				}
				else
				{
					$contador = 0;
					for($y=1;$y<=25;$y++)
					{
						if(definirLinha($indice) == definirLinha($y))
						{
							if($y == $indice+1 || $y == $indice-1)
							{
								if(is_a($this->tabela[$y], 'Peca'))
								{
									if($this->tabela[$y]->lado != 'computador')
									{
										$movimentos[$contador] = $y;
										$contador++;
									}
								}
								else if(is_a($this->tabela[$y], 'Objeto'))
								{
									if($this->tabela[$y]->classe == 'casa')
									{
										$movimentos[$contador] = $y;
										$contador++;
									}
								}
							}		
						}
						else if($y == ($indice - 5) || $indice == ($y - 5))
							{
								if(is_a($this->tabela[$y], 'Peca'))
								{
									if($this->tabela[$y]->lado != 'computador')
									{
										$movimentos[$contador] = $y;
										$contador++;
									}
								}
								else if(is_a($this->tabela[$y], 'Objeto'))
								{
									if($this->tabela[$y]->classe == 'casa')
									{
										$movimentos[$contador] = $y;
										$contador++;
									}
								}
								
							}
						}

					}

		shuffle($movimentos);
		$indicedestino = $movimentos[0];
		$resultado = $this->tabela[$indice]->LutarPC($this->tabela[$indicedestino]);

		if($resultado == 0)
		{
			$c = new Objeto('casa');
			$this->tabela[$indice] = $c;
		}
		else if($resultado == 1)
		{
			$c = new Objeto('casa');
			$this->tabela[$indicedestino] = $this->tabela[$indice];
			$this->tabela[$indice] = $c;

		}
		else if($resultado == 2)
		{
			$c = new Objeto('casa');
			$this->tabela[$indice] = $c;
			$this->tabela[$indicedestino] = $c;
		}
		else if($resultado == 9)
		{
			$this->on = false;
		}

		$m = $this->contarPecas('computador');
		$n = $this->contarPecas('jogador');
		if($m > 0)
		{
			if($n > 0)
			{
				$this->exibirTabuleiro();
			}
			else
			{
				echo "<script> alert('As suas peças móveis acabaram, você perdeu') </script>";
				$this->on = false;
			}
		}
		else
		{
			echo "<script> alert('As peças móveis de seu adversário acabaram, você venceu!') </script>";
			$this->on = false;
		}

		if($this->on == false)
	{
		echo "
	    <div id='opcao'>
		<p><h1><center> O jogo acabou<br> O que deseja fazer? </center></h1></p>
		<center><button id='novo' class='botaoentrar'> Iniciar Novo Jogo </button>
		<button id='reiniciar' class='botaoentrar' > Reiniciar </button></center>
		<form name='reiniciar' action='jogo.php' method='GET'>
		<input type='hidden' id='reiniciar' value='sim'>
		</form>
		<br><br><br>
		</div>";

	}

		

	




	
}

}

?>
